<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatemahasiswaAPIRequest;
use App\Http\Requests\API\UpdatemahasiswaAPIRequest;
use App\Models\mahasiswa;
use App\Repositories\mahasiswaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class mahasiswaController
 * @package App\Http\Controllers\API
 */

class mahasiswaAPIController extends AppBaseController
{
    /** @var  mahasiswaRepository */
    private $mahasiswaRepository;

    public function __construct(mahasiswaRepository $mahasiswaRepo)
    {
        $this->mahasiswaRepository = $mahasiswaRepo;
    }

    /**
     * Display a listing of the mahasiswa.
     * GET|HEAD /mahasiswas
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->mahasiswaRepository->pushCriteria(new RequestCriteria($request));
        $this->mahasiswaRepository->pushCriteria(new LimitOffsetCriteria($request));
        $mahasiswas = $this->mahasiswaRepository->all();

        return $this->sendResponse($mahasiswas->toArray(), 'Mahasiswas retrieved successfully');
    }

    /**
     * Store a newly created mahasiswa in storage.
     * POST /mahasiswas
     *
     * @param CreatemahasiswaAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatemahasiswaAPIRequest $request)
    {
        $input = $request->all();

        $mahasiswa = $this->mahasiswaRepository->create($input);

        return $this->sendResponse($mahasiswa->toArray(), 'Mahasiswa saved successfully');
    }

    /**
     * Display the specified mahasiswa.
     * GET|HEAD /mahasiswas/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var mahasiswa $mahasiswa */
        $mahasiswa = $this->mahasiswaRepository->findWithoutFail($id);

        if (empty($mahasiswa)) {
            return $this->sendError('Mahasiswa not found');
        }

        return $this->sendResponse($mahasiswa->toArray(), 'Mahasiswa retrieved successfully');
    }

    /**
     * Update the specified mahasiswa in storage.
     * PUT/PATCH /mahasiswas/{id}
     *
     * @param  int $id
     * @param UpdatemahasiswaAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatemahasiswaAPIRequest $request)
    {
        $input = $request->all();

        /** @var mahasiswa $mahasiswa */
        $mahasiswa = $this->mahasiswaRepository->findWithoutFail($id);

        if (empty($mahasiswa)) {
            return $this->sendError('Mahasiswa not found');
        }

        $mahasiswa = $this->mahasiswaRepository->update($input, $id);

        return $this->sendResponse($mahasiswa->toArray(), 'mahasiswa updated successfully');
    }

    /**
     * Remove the specified mahasiswa from storage.
     * DELETE /mahasiswas/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var mahasiswa $mahasiswa */
        $mahasiswa = $this->mahasiswaRepository->findWithoutFail($id);

        if (empty($mahasiswa)) {
            return $this->sendError('Mahasiswa not found');
        }

        $mahasiswa->delete();

        return $this->sendSuccess('Mahasiswa deleted successfully');
    }
}
