<?php

namespace App\Repositories;

use App\Models\mahasiswa;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class mahasiswaRepository
 * @package App\Repositories
 * @version April 8, 2020, 1:09 pm UTC
 *
 * @method mahasiswa findWithoutFail($id, $columns = ['*'])
 * @method mahasiswa find($id, $columns = ['*'])
 * @method mahasiswa first($columns = ['*'])
*/
class mahasiswaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'nim'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return mahasiswa::class;
    }
}
