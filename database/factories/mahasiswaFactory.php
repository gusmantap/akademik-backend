<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\mahasiswa;
use Faker\Generator as Faker;

$factory->define(mahasiswa::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'nim' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
