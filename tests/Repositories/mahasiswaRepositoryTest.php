<?php namespace Tests\Repositories;

use App\Models\mahasiswa;
use App\Repositories\mahasiswaRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class mahasiswaRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var mahasiswaRepository
     */
    protected $mahasiswaRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->mahasiswaRepo = \App::make(mahasiswaRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_mahasiswa()
    {
        $mahasiswa = factory(mahasiswa::class)->make()->toArray();

        $createdmahasiswa = $this->mahasiswaRepo->create($mahasiswa);

        $createdmahasiswa = $createdmahasiswa->toArray();
        $this->assertArrayHasKey('id', $createdmahasiswa);
        $this->assertNotNull($createdmahasiswa['id'], 'Created mahasiswa must have id specified');
        $this->assertNotNull(mahasiswa::find($createdmahasiswa['id']), 'mahasiswa with given id must be in DB');
        $this->assertModelData($mahasiswa, $createdmahasiswa);
    }

    /**
     * @test read
     */
    public function test_read_mahasiswa()
    {
        $mahasiswa = factory(mahasiswa::class)->create();

        $dbmahasiswa = $this->mahasiswaRepo->find($mahasiswa->id);

        $dbmahasiswa = $dbmahasiswa->toArray();
        $this->assertModelData($mahasiswa->toArray(), $dbmahasiswa);
    }

    /**
     * @test update
     */
    public function test_update_mahasiswa()
    {
        $mahasiswa = factory(mahasiswa::class)->create();
        $fakemahasiswa = factory(mahasiswa::class)->make()->toArray();

        $updatedmahasiswa = $this->mahasiswaRepo->update($fakemahasiswa, $mahasiswa->id);

        $this->assertModelData($fakemahasiswa, $updatedmahasiswa->toArray());
        $dbmahasiswa = $this->mahasiswaRepo->find($mahasiswa->id);
        $this->assertModelData($fakemahasiswa, $dbmahasiswa->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_mahasiswa()
    {
        $mahasiswa = factory(mahasiswa::class)->create();

        $resp = $this->mahasiswaRepo->delete($mahasiswa->id);

        $this->assertTrue($resp);
        $this->assertNull(mahasiswa::find($mahasiswa->id), 'mahasiswa should not exist in DB');
    }
}
